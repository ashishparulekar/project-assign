const Sequelize=require('sequelize')
const sequelize=new Sequelize('node1','postgres','abc123', { host:'localhost',dialect:'postgres'})

// database connection checking
sequelize.authenticate().then(() => {
    console.log('Connected');
})
.catch(err => {
     console.error('Unable to connect ', err);
  })

  module.exports=sequelize;

/*{
  "development": {
    "username": "postgres",
    "password": "abc123",
    "database": "node1",
    "host": "localhost",
    "dialect": "postgres",
    "port":"5432",
    "operatorsAliases": false
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false
  }
}
 */