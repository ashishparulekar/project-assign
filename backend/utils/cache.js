// Load required packages

const redis = require('redis');

const client = redis.createClient();
//const client = redis.createClient('6379', 'redis');


module.exports = (data) => {
    return client.expireat(data, 20);
};