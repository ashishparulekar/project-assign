const Category = require('./category.js')
const Sequelize = require('sequelize')
const sequelize = require('../../config/config.js')

//product table
const Product = sequelize.define('product', {
    productId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },

    productName: { type: Sequelize.STRING, unique: true, allowNull: false },
    
    productPrice : { type: Sequelize.INTEGER, allowNull: false },

    productImage : { type: Sequelize.TEXT },

    categoryId : { type: Sequelize.INTEGER, allowNull: false },

    categoryName : { type: Sequelize.STRING, allowNull: false },

    //cartId : {type : Sequelize.INTEGER , allowNull : false},

    createdBy : { type: Sequelize.STRING, allowNull: false }
},
    { freezeTableName: true, tableName: 'product' })


    Product.associate = function (models) {
        Product.belongsTo(models.category, { foreignKey: 'categoryId', as: 'category' });
        //Product.belongsTo(models.cart , {foreignKey : 'cartId', as : 'cart'})
    }

module.exports = Product