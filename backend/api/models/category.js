const Sequelize = require('sequelize')
const sequelize = require('../../config/config.js')

//category table
const Category = sequelize.define('category', {
    categoryId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false , autoIncrement: true },

    categoryName: { type: Sequelize.STRING,unique:true, allowNull: false },

    createdBy : { type: Sequelize.STRING, allowNull: false }

},
    { freezeTableName: true, tableName: 'category' })


module.exports = Category