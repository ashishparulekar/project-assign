const Sequelize = require('sequelize')
const sequelize = require('../../config/config.js')
const bcrypt = require('bcrypt')

//user table
const User = sequelize.define('user', {
    userId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },

    userName : { type: Sequelize.STRING, allowNull: false },

    userEmail : { type: Sequelize.STRING,unique: true, allowNull: false },

    number: { type: Sequelize.INTEGER,unique: true, allowNull: false },

    password: { type: Sequelize.STRING, allowNull: false },

    roleId: { type: Sequelize.INTEGER,allowNull: false },

    token: { type: Sequelize.STRING, allowNull: true }
},
    { freezeTableName: true, tableName: 'user' });

     User.associate = function (models) {
         User.belongsTo(models.Roles, { foreignKey: 'roleId', as: 'role' });
     }


User.beforeCreate((user) => {
    return bcrypt.hash(user.password, 10)
        .then(hash => {
            user.password = hash
        })
        .catch(err => {
            throw new Error();
        });
});

User.beforeUpdate((user) => {
    return bcrypt.hash(user.password, 10)
        .then(hash => {
            user.password = hash
        })
        .catch(err => {
            throw new Error();
        });
});

module.exports = User