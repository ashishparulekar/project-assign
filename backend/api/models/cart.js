const Sequelize = require('sequelize');
const sequelize = require('../../config/config.js');

const Cart = sequelize.define('cart',{
    userId : { type : Sequelize.INTEGER},

    productId : { type : Sequelize.INTEGER},

    productName : { type : Sequelize.STRING},

    //categoryId : { type : Sequelize.INTEGER },

    //categoryName : { type : Sequelize.STRING}
},

    {freezeTableName : true , tableName : 'cart'}

)

Cart.associate = function (models) {
    Cart.belongsTo(models.product, { foreignKey: 'productId', as: 'product' });
    //Cart.belongsTo(models.category, { foreignKey: 'productId', as: 'category' });
}

module.exports = Cart;