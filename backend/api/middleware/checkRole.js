const jwt = require('jsonwebtoken')
const User = require('../models/user.js')

exports.admin = async (req, res, next) => {
    try {

        let token = req.headers.authorization.split(' ')[1]

        let decode = jwt.decode(token, 'key')
        console.log(decode)

        let userData = await User.findOne( { where : { userEmail: decode.userEmail, roleId:decode.roleId, token: token } } );

        if (userData.roleId == 1) {
            req.userEmail = decode.userEmail;
            next()
        } else {
            res.status(401).send({ message: 'unautorized' })
        }
    } catch (err) {
        res.status(401).send({ message: 'unautorized' })

    }

}

exports.supervisor = async (req, res, next) => {
    try {

        let token = req.headers.authorization.split(' ')[1]

        let decode = jwt.decode(token, 'key')
        console.log(decode)

        let userData = await User.findOne( { where :{ userEmail: decode.userEmail, roleId:decode.roleId, token: token } } );

        if (userData.roleId == 2) {
            req.userEmail = decode.userEmail;
            next()
        } else {
            res.status(401).send({ message: 'unautorized' })
        }
    } catch (err) {
        res.status(401).send({ message: 'unautorized' })

    }

}
