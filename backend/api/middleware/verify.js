const redis = require('redis')

const jwt = require('jsonwebtoken')
const User = require('../models/user.js')


const client = redis.createClient();
client.on('connect', function() {
    console.log('connected');
  });


exports.decode = async (req, res, next) => {
    try {

        let token = req.headers.authorization.split(' ')[1]

        let decode = jwt.decode(token, 'key')
        console.log(decode)

        let userData = await User.findOne( { where :{ userEmail: decode.userEmail, roleId:decode.roleId, token: token } } );

        if (userData) {
            next()
        } else {
            res.status(401).send({ message: 'unautorized' })
        }
    } catch (err) {
        res.status(401).send({ message: 'unautorized' })

    }

}

