const { body } = require('express-validator');

exports.RefreshTokenValidation = [
    body('userName')
        .exists()
        .withMessage('Username is Required'),

    body('password')
        .exists()
        .withMessage('Password is Required')
];


exports.AccessTokenValidation = [
    body('refreshToken')
        .exists()
        .withMessage('Refresh Token is Required')
];