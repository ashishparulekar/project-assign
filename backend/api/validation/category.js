const { body } = require("express-validator");
const models = require("../models/");
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

exports.categoryValidation = [
  body('categoryName')
    .exists().withMessage('Category name is required')
    .custom(async value => {
      return await models.category.findOne({ where: { 
        categoryName: {
          [Op.iLike]: value},
          isActive: true }}).then(category => {
        if (category) {
          return Promise.reject("Category name already exist !");
        }
      })
    })
    

];
