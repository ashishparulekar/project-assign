const Category = require('../models/category.js')
const Product = require('../models/product.js')

Product.belongsTo(Category, { foreignKey: 'categoryId' })

exports.ReadCategory = async (req, res) => {
    try {
        let getCategory = await Category.findAll()
        if (getCategory.length === 0) {
            res.status(404).send({ error: 'category not found' });
        }
        else {
            res.status(200).send(getCategory)
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.ReadCategoryById = async (req, res) => {
    try {
        //as semicolon(:) is used we use req.params to get data
        const categoryId = req.params.categoryId
        let getCategoryById = await Category.findAll({ include: Product, attributes: ['categoryName'], where: { categoryId: categoryId } })
        if (getCategoryById.length === 0) {
            res.status(404).send({ message: "Category not found" })
        } else {
            res.send(getCategoryById)
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.ReadCategoryByName = async (req, res) => {
    try {
        const categoryName = req.params.categoryName
        let getCategoryByName = await Category.findAll({ include: Product, attributes: ['categoryId'], where: { categoryName: categoryName } })
        if (getCategoryByName.length === 0) {
            res.status(404).send({ message: "Category not found" })
        } else {
            res.send(getCategoryByName)
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}


exports.AddCategory = async (req, res) => {
    try {
        const {categoryName,createdBy } = req.body
        let addCategory = await Category.create({ categoryName: categoryName,createdBy:createdBy })
        if (addCategory) {
            res.status(201).send(addCategory);
        }
        else {
            res.status(422).json({ error: "Category Not Added"})
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.UpdateCategory = async (req, res) => {
    try {
        const { categoryId, categoryName } = req.body
        let updateCategory = await Category.update({ categoryName: categoryName }, { where: { categoryId: categoryId } })
        if (updateCategory) {
            res.status(200).send({message: 'category updated' });
        }
        else {
            res.status(404).send({ error: 'category not found' });
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.DeleteCategoryById = async (req, res) => {
    try {
        const { categoryId } = req.body
        let deleteCategory = Category.destroy({ where: { categoryId: categoryId } })
        if (!deleteCategory) {
            res.status(404).send({ error: 'category not found' });
        }
        else {
            res.status(200).send({ message: 'category deleted' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.DeleteCategoryByName = async (req, res) => {
    try {
        const { categoryName } = req.body
        let deleteCategory = Category.destroy({ where: { categoryName: categoryName } })
        if (!deleteCategory) {
            res.status(404).send({ error: 'category not found' });
        }
        else {
            res.status(200).send({ message: 'category deleted' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.ReadCategoryByPagination = async(req,res)=>{
     try
     {
         const categoryPerPage=2;
         // as semicolon(:) will be used, so req.params is used to get data
         const page = req.params.index
         const offset=(page-1)*categoryPerPage
         const limit=categoryPerPage
         let getCategoryByPagination = await Category.findAll({offset:offset, limit:limit ,attributes:['categoryId','categoryName','createdBy']})
         if(getCategoryByPagination.length === 0 )
         {
             res.status(404).send({ error: 'data not found on this page ',page });
         }
         else
         {
             res.status(200).send(getCategoryByPagination)
         }
    }
     catch(err)
     {
         console.log(err);
         res.status(500).send(err);
     }
 }
