const Roles = require('../models/roles.js')

exports.AddRole = async (req, res) => {
    try {
        const { roleName } = req.body
        let postRole = await Roles.create({ roleName: roleName })
        if (!postRole) {
            res.status(422).send({ error: 'Role not created' });
        }
        else {
            res.status(201).json({ message: "Role Added", data: postRole })
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

/*exports.deleteRoles = async (req, res) => {
    try {
        const { roleId } = req.body
        let delRole = await Roles.destroy({where: {roleId: roleId} })
        if (!delRole) {
            res.status(422).send({ error: 'Role not deleted' });
        }
        else {
            res.status(201).json({ message: "Role deleted"})
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
}*/

exports.DeleteRole = async (req, res) => {
    try {
        const { roleName } = req.body
        let deleteRole = await Roles.destroy({ where: { roleName: roleName } })
        if (!deleteRole) {
            res.status(404).send({ error: 'data not found' });
        }
        else {
            res.status(200).send({ message: 'role deleted' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}
