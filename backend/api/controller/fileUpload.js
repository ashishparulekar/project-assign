//const express = require('express');
//const FileUpload = require('../models/fileUpload.js')
const multer = require('multer');
//const upload = multer({dest:'uploads/'}).single("blackhat_image");

var storage = multer.diskStorage({   
    destination: function(req, file, cb) { 
       cb(null, './uploads/');    
    }, 
    filename: function (req, file, cb) { 
       cb(null , file.originalname);   
    }
 });

const upload = multer({ storage: storage }).single('blackhat_image');
const uploads = multer({ storage: storage }).array("demo_images", 6);


exports.AddFileUpload = (req, res) => {
    upload(req, res, (err) => {
     if(err) {
       res.status(400).send("Something went wrong!");
     }
     res.send(req.file);
   });
};

exports.AddFileUploads = (req, res) => {
   uploads(req,res,(err) => {
      if(err){
         res.status(400).send('Something went wrong');
      }
      res.send(req.files);
   });
};



