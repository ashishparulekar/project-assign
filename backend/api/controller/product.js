const Product=require('../models/product.js')
const Category=require('../models/category.js')
const  Sequelize  = require('sequelize')
const Op = Sequelize.Op

//const multer = require('multer');

Category.hasMany(Product, { foreignKey: 'categoryId' })

exports.ReadProducts = async (req,res) => {
    try
    {
        let getProducts=await Product.findAll()
        if(getProducts.length===0)
        {
            res.status(404).send({ error: 'Products not found' });
            console.log(error)
        }
        else
        {
            res.status(200).send(getProducts)
        }
    }
    catch(err)
    {
        res.status(500).send(err);
    }
}
/*var storage = multer.diskStorage({   
    destination: function(req, file, cb) { 
       cb(null, './uploads');    
    }, 
    filename: function (req, file, cb) { 
       cb(null , file.originalname);   
    }
 });*/

//const upload = multer({dest:'uploads/'});
//const upload = multer({ storage: storage }).single('black_image');

exports.AddProducts = async (req,res) => {
    try
    {   
        const { productId, productName ,productPrice ,productImage ,categoryId ,categoryName ,createdBy } = req.body
        let postProducts = await Product.create( { productId:productId,productName:productName ,productPrice:productPrice,productImage:productImage, categoryId:categoryId ,categoryName:categoryName,createdBy:createdBy } )
        if(!postProducts)
        {
            res.status(422).send({ error: 'Product not created' });
            console.log(error)
        }
        else
        {
            res.status(201).json( { message: "Product Added", data : postProducts } )
        }
    }
    catch(err)
    {
        res.status(500).send(err);
    }
}

exports.UpdateProduct = async(req,res) => {
    try{
            const {  productId, productName,productPrice,productImage,categoryId,categoryName,createdBy} = req.body
            let updateProduct=await Product.update( { productName:productName ,productPrice:productPrice,productImage:productImage, categoryId:categoryId,categoryName:categoryName,createdBy:createdBy } , { where : { productId:productId } } )
            if(!updateProduct)
            {
                res.status(404).send({ error: 'Product not found' });
            }
            else
            {
                res.status(200).send({ message: 'Product updated' });
            }
        }
    catch(error)
    {
        res.status(500).send(error);
    }
}

exports.DeleteProductById = async (req, res) => {
    try {
        const { productId } = req.body
        let deleteProductById = await Product.destroy({ where: { productId: productId } })
        if (!deleteProductById) {
            res.status(404).send({ error: 'data not found' });
        }
        else {
            res.status(200).send({ message: 'Product deleted' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.DeleteProductByName = async (req, res) => {
    try {
        const { productName } = req.body
        let deleteProductByName = await Product.destroy({ where: { productName : productName } })
        if (!deleteProductByName) {
            res.status(404).send({ error: 'data not found' });
        }
        else {
            res.status(200).send({ message: 'Product deleted' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.ReadProductsByPagination = async(req,res)=>{
    try
    {
        const productsPerpage = 1;
        // as we are using semicolon(:) so req.params
        const pages = req.params.pagination
        const offset=(pages-1)*productsPerpage
        const limit=productsPerpage
        let getProductsByPagination=await Product.findAll( {include:Category, offset:offset, limit:limit ,attributes: ['productId','productName','productPrice','productImage','categoryId','categoryName','createdBy']  })
        if(getProductsByPagination.length === 0 )
        {
            res.status(404).send({ error: 'data not found on this page ',pages });
        }
        else
        {
            res.status(200).send(getProductsByPagination)
        }
    }
    catch(error)
    {
        res.status(500).send(error);
    }
}

exports.ReadProductsByPrice= async (req,res)=>{
    try
    {
        const {priceStart,priceEnd} = req.body
        let getProducts=await Product.findAll( { where : { price :{[Op.between]:[priceStart,priceEnd]} } } )
        if(getProducts.length===0)
        {
            res.status(404).send({ error: 'Product not found' });
            console.log(error)
        }
        else
        {
            res.status(200).send(getProducts)
        }
    }
    catch(err)
    {
        res.status(500).send(err);
    }
}