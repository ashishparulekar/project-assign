const multer = require('multer');
xlsxtojson = require('xlsx-to-json');

exceltojson = xlsxtojson;
excel2json = xlsxtojson;

var storage = multer.diskStorage({
    destination: function(req, file,cb){
        cb(null,"./uploads/bulkupload");  
    },
    filename:  function( req, file, cb){
        cb(null,file.originalname);
    }
});

const upload = multer({storage}).single('bulkupload');
//const uploads = multer({ storage }).array("demo_upload", 6);


exports.AddBulkUpload = async (req,res)=>{
    upload(req,res,async (err)=>{
        if(!upload){
            res.status(400).json({message:'error while file uploading'});
        }
        else{
            exceltojson({
                input: req.file.path,
                output:"uploads/bulkupload/"+Date.now()+".json",
                lowerCaseHeaders:true
            },function(err,result){
                if(err){
                    res.json(err);
                }
                else{
                    res.json(result);
                }
            
            });
        }
    })
}

/*exports.postbulkUploads = async (req,res)=>{
    uploads(req,res,async (err)=>{
        if(!uploads){
            res.status(400).json({message:'error while file uploading'});
        }
        else{
            excel2json({
                input: req.file.path,
                output:"uploads/bulkupload/"+Date.now()+".json",
                lowerCaseHeaders:true
            },function(err,results){
                if(err){
                    res.json(err);
                }
                else{
                    res.json(results);
                }
            
            });
        }
    })
}*/