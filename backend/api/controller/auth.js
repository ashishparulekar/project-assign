const jwt = require('jsonwebtoken')
const User = require('../models/user.js')
const bcrypt = require('bcrypt')

exports.userLogin = async (req, res) => {
    try {
        const { userEmail, password } = req.body;
        let userData = await User.findOne({ where: { userEmail: userEmail } });
        if (userData) {
            let pass = await bcrypt.compare(password, userData.dataValues.password)

            if (pass) {
                let token = await jwt.sign({ userEmail: userData.dataValues.userEmail , roleId:userData.dataValues.roleId },'key');
                await User.update({ token: token }, { where: { userEmail: userData.dataValues.userEmail } })
                res.status(200).send({ token })

            } else {
                res.status(404).send({ message: 'Password is not correct' })
            }
        }
        else {
            res.send(" User not found");
        }
    } catch (err) {
        res.send(err)
    }

}