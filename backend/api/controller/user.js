const User = require('../models/user.js')
const bcrypt = require('bcrypt')

exports.ReadUser = async (req, res) => {
    try {
        let getUser = await Category.findAll()
        if (getUser.length === 0) {
            res.status(404).send({ error: 'user not found' });
        }
        else {
            res.status(200).send(getUser)
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.ReadUserById = async (req, res) => {
    try {
        //as we are using semicolon(:) we use req.params
        const userId = req.params.userId;
        let getUserById = await Category.findAll({ include: User, attributes: ['userName','userEmail','number'], where: { userId: userId } })
        if (getUserById.length === 0) {
            res.status(404).send({ error: 'user not found' });
        }
        else {
            res.status(200).send(getUserById)
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.AddUser = async (req, res) => {
    try {
        const { userName , userEmail ,number, password , roleId } = req.body
        let postUser = await User.create({ userName: userName,userEmail:userEmail,number:number, password: password,roleId:roleId })
        if (!postUser) {
            res.status(422).send({ error: 'User not created' });
        }
        else {
            res.status(201).json({ message: "User Added", data: postUser })
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.UpdateUser = async (req, res) => {
    try {
        const { userEmail, oldPassword, newPassword } = req.body
        let user = await User.findOne({ where: { userEmail : userEmail } })

        if (!user) {
            res.status(404).send({ error: 'User not found' });
        }
        else {
            let checkPassword = await bcrypt.compare(oldPassword, user.dataValues.password)
            if (!checkPassword) {
                res.status(422).send({ error: 'invalid password' });
            }
            else {
                let updatePassword = await user.update({ password: newPassword }, { where: { emailId: user.dataValues.emailId } })
                if (!updatePassword) {
                    res.status(422).send({ error: 'Password not updated' });
                }
                else {
                    res.status(201).send("Password Updated")
                }
            }

        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

exports.DeleteUser = async (req, res) => {
    try {
        const { userName } = req.body
        let deleteUser = await User.destroy({ where: { userName: userName } })
        if (!deleteUser) {
            res.status(404).send({ error: 'user not found' });
        }
        else {
            res.status(200).send({ message: 'user deleted' });
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}
