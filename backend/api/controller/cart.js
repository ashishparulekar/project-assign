const Cart = require('../models/cart.js');
const Product = require('../models/product.js');

Cart.hasMany(Product, { foreignKey: 'cartId' })
