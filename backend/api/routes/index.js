const express = require('express');
var router = express.Router();

const authRoute = require('./auth');
const bulkUploadRoute = require('./bulkUpload');
const categoryRoute = require('./category');
const fileUploadRoute = require('./fileUpload');
const productRoute = require('./product');
const rolesRoute = require('./roles');
const userRoute = require('./user');

router.use('/auth', authRoute);
router.use('/bulk-Upload',bulkUploadRoute);
router.use('/category', categoryRoute);
router.use('/file-Upload',fileUploadRoute);
router.use('/product', productRoute);
router.use('/roles', rolesRoute);
router.use('/user', userRoute);

module.exports = router;