const express = require('express');
const router = express.Router();

const fileUpload=require('../controller/fileUpload.js');

router.post('/file-upload',fileUpload.AddFileUpload);
router.post('/file-uploads',fileUpload.AddFileUploads)


module.exports = router;