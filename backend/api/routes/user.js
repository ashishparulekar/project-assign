const express = require('express');
const router = express.Router();
const user = require('../controller/user.js');

router.get('/get-user',user.ReadUser);
router.get('/get-user/:userId',user.ReadUserById);
router.post('/add-user', user.AddUser);
router.put('/update-user', user.UpdateUser);
router.delete('/delete-user',user.DeleteUser);

module.exports = router;