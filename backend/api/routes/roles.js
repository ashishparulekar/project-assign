const express = require('express');
const router = express.Router();
const roles = require('../controller/roles.js');

router.post('/add-role', roles.AddRole);
router.delete('/delete-role',roles.DeleteRole);

module.exports = router;