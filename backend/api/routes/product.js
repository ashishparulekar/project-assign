const express = require('express');
const   router = express.Router();

//const checkRole=require('../middleware/checkRole.js')
const product=require('../controller/product.js');

router.get('/get-product',product.ReadProducts);
router.post('/add-product',product.AddProducts);
router.put('/update-product', product.UpdateProduct);
router.delete('/delete-product-id', product.DeleteProductById);
router.delete('/delete-product-name',product.DeleteProductByName);
router.get('/get-product/:pagination',product.ReadProductsByPagination);
router.get('/get-product-by-price',product.ReadProductsByPrice);


module.exports=router;