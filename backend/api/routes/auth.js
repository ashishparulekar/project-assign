const express = require('express');
const router = express.Router();
const authentication=require('../controller/auth');

router.post('/',authentication.userLogin);

module.exports=router;