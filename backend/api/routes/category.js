const express = require('express');
const  router = express.Router();
//const checkRole=require('../middleware/checkRole.js')

const category=require('../controller/category.js');

router.get('/get-category',category.ReadCategory);
router.get('/get-categoryId/:categoryId',category.ReadCategoryById);
router.get('/get-categoryName/:categoryName',category.ReadCategoryByName);
router.post('/add-category',category.AddCategory);
router.put('/update-category',category.UpdateCategory);
router.delete('/delete-category-id',category.DeleteCategoryById);
router.delete('/delete-category-name',category.DeleteCategoryByName);
router.get('/get-category/:index',category.ReadCategoryByPagination);

module.exports = router;