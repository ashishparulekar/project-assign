
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http'

import { ViewComponent } from './view/view.component';
import { NavbarComponent } from './view/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './view/register/register.component';
import { LoginComponent } from './view/login/login.component';
import { HomeComponent } from './view/home/home.component';
import { ViewRoutingModule } from './view/view-routing.module';
import { CategoryComponent } from './view/category/category.component';
import { CategoryDataComponent } from './view/category-data/category-data.component';
import { ProductComponent } from './view/product/product.component';
import { ProductDataComponent } from './view/product-data/product-data.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    CategoryComponent,
    CategoryDataComponent,
    ProductComponent,
    ProductDataComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    ViewRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
