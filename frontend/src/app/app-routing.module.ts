import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './view/register/register.component';
import { LoginComponent } from './view/login/login.component';
//import { AppComponent } from './app.component';
//import { ViewComponent } from './view/view.component';
import { HomeComponent } from './view/home/home.component';
import { CategoryComponent } from './view/category/category.component';
import { CategoryDataComponent } from './view/category-data/category-data.component';
import { ProductComponent } from './view/product/product.component';
import { ProductDataComponent } from './view/product-data/product-data.component';


const routes: Routes = [
  {
    path : '',
    redirectTo : 'home',
    pathMatch : 'full'
  },
  {
    path : 'home' , component : HomeComponent
  },
  {
    path: 'register' , component : RegisterComponent
  },
  {
    path: 'login' , component : LoginComponent
  },
  {
    path : 'category' , component : CategoryComponent
  },
  {
    path : 'category-data' , component : CategoryDataComponent
  },
  {
    path : 'product' , component : ProductComponent
  },
  {
    path : 'product-data' , component : ProductDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
