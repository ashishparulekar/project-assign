import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { CategoryService } from "../category/category.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-category-data',
  templateUrl: './category-data.component.html',
  styleUrls: ['./category-data.component.scss']
})
export class CategoryDataComponent implements OnInit {

  categoryData : any = [];

  dataSource: MatTableDataSource<CategoryService>;
  displayedColumns: string[] = ['categoryId', 'categoryName', 'createdBy','action'];

  
  constructor(private http:CategoryService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCategoryData();
  }

  getCategoryData(){
    this.http.getCategoryData().subscribe(data => {
      this.categoryData = data;
      this.dataSource = new MatTableDataSource<CategoryService>(this.categoryData);
    })
   
  }

  deleteCategory(categoryId: number) {
    this.http.deleteCategoryData(categoryId).subscribe(response=>
      {
        console.log('response from delete data is',response)
      });
  }

}
