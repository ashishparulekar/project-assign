export class RegisterModel{
    userName : String;
    userEmail : String;
    number : Number;
    password : String;
    roleId : Number;
}