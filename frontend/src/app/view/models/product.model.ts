export class ProductModel{
    productId : Number;
    productName : String;
    productPrice : Number;
    productImage : String;
    categoryId : Number;
    categoryName : String;
    createdBy : String;
}