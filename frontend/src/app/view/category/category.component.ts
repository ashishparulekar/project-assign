import { Component, OnInit } from '@angular/core';
import { CategoryModel } from '../models/category.model';
import { FormGroup , FormBuilder , Validators, FormControl} from '@angular/forms';
import { CategoryService } from '../category/category.service';
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {


  categoryData: any = [];

  dataSource: MatTableDataSource<CategoryService>;
  displayedColumns: string[] = ['categoryId','categoryName', 'createdBy','action'];

  user : CategoryModel = new CategoryModel;
  categoryForm : FormGroup;
  hide : true;
  alert : boolean = false;

  constructor(private formBuilder:FormBuilder, private http : CategoryService) { }

  ngOnInit(): void {
    this.categoryForm = new FormGroup({
      categoryId : new FormControl('',[Validators.required]),
      categoryName : new FormControl('',[Validators.required]),
      createdBy : new FormControl('',[Validators.required])
    })
  }

  onCategorySubmit(){
    alert("category added successfully")
    this.http.postCategoryData(this.categoryForm.value).subscribe((res) => {
      console.log('response from post data is ',res);
      this.alert = true;
    },(err) =>{
      console.log('error during post data is',err)
    })
    this.categoryForm.reset({})
   }
   closealert(){
     this.alert = false;
  }

  getCategoryData(){
    this.http.getCategoryData().subscribe(data => {
      this.categoryData = data;
      this.dataSource = new MatTableDataSource<CategoryService>(this.categoryData);
    })
  }
}


