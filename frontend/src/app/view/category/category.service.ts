import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const apiUrl = "http://localhost:5000/category";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http : HttpClient) { }

  postCategoryData(data){
    return this.http.post(apiUrl+'/add-category',data)
  }

  getCategoryData():Observable<Object>{
    return this.http.get(apiUrl+'/get-category')
  }

  // deleteCategoryData(categoryId: number): Observable<any> {
  //   return this.http.delete(`${apiUrl+'/delete-category-id'}/${categoryId}`);
  // }
  deleteCategoryData(categoryId: number): Observable<any> {
    const options = {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      }),
      body : { categoryId : categoryId}
    };
    return this.http.delete<any>(
      apiUrl + '/delete-category-id',
      options
    )
  }
}
