import { Component, OnInit } from '@angular/core';
import { ProductModel } from '../models/product.model';
import { FormGroup , FormBuilder , Validators, FormControl} from '@angular/forms';
import { ProductService } from '../product/product.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  productData : any = [];

  dataSource : MatTableDataSource<ProductService>;
  displayedColumns : string[] = ['productId','productName','productPrice','productImage','categoryId','categoryName','createdBy','action'];


  user : ProductModel = new ProductModel;
  productForm : FormGroup;
  alert : boolean=false;

  constructor(private formBuilder : FormBuilder, private http : ProductService) { }

  ngOnInit(): void {
    this.productForm = new FormGroup({
      productId : new FormControl('',[Validators.required]),
      productName : new FormControl('',[Validators.required]),
      productPrice : new FormControl('',[Validators.required]),
      productImage : new FormControl('',[Validators.required]),
      categoryId : new FormControl('', [Validators.required]),
      categoryName : new FormControl('' , [Validators.required]),
      createdBy : new FormControl('' , [Validators.required])
    })
  }

  onProductSubmit(){
    alert("product added successfully!")
    this.http.postProductData(this.productForm.value).subscribe((res) => {
      console.log('data from post req is',res);
      this.alert = true;
    },(err) => {
      console.log('error from post req is', err);
    })
    this.productForm.reset({});
  }
  closealert(){
    this.alert = false;
  }

  getProductData(){
    this.http.getProductData().subscribe(data => {
      this.productData = data;
      this.dataSource = new MatTableDataSource<ProductService>(this.productData)
    })
  }
    
}
