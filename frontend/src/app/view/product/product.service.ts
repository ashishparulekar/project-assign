import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const apiUrl = "http://localhost:5000/product";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http : HttpClient) { }

  postProductData(data){
    return this.http.post(apiUrl+'/add-product',data);
  }

  getProductData():Observable<Object>{
    return this.http.get(apiUrl+'/get-product')
  }

  deleteProductData(productId : number): Observable<any> {
    const options = {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      }),
      body : {productId : productId}
    };
    return this.http.delete<any>(
      apiUrl + '/delete-product-id',
      options
    )
  }
}
