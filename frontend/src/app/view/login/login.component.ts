import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../models/login.model';
import { FormGroup , FormBuilder , Validators , FormControl} from '@angular/forms';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: LoginModel = new LoginModel();
  loginForm : FormGroup;
  hide = true;
  alert : boolean = false;

  constructor(private formBuilder : FormBuilder, private http : LoginService) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      userEmail: new FormControl('' , [Validators.required,Validators.email]),
      password: new FormControl('' , [Validators.required,Validators.minLength(8),Validators.maxLength(15)])
    }) 
  }

  onLoginSubmit(){
    alert('login successful')
    this.http.postLoginData(this.loginForm.value).subscribe((res) => {
      console.log('response from post data is ',res);
      this.alert = true;
    },(err) =>{
      console.log('error during post data is',err)
    })
    this.loginForm.reset({})
   }
   closealert(){
     this.alert = false;
  }
}


