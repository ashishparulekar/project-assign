import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ProductService } from '../product/product.service';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

@Component({
  selector: 'app-product-data',
  templateUrl: './product-data.component.html',
  styleUrls: ['./product-data.component.scss']
})
export class ProductDataComponent implements OnInit {

  productData : any = [];

  datasource: MatTableDataSource<ProductService>;
  displayedColumns: string[] = ['productId', 'productName', 'productPrice', 'productImage', 'categoryId', 'categoryName', 'createdBy','action'];


  constructor(private http : ProductService, private dialog:MatDialog) { }

  ngOnInit(): void {
    this.getProductData();
  }

  getProductData(){
    this.http.getProductData().subscribe(data => {
      this.productData = data;
      this.datasource = new MatTableDataSource<ProductService>(this.productData);
    })
  }

  deleteProduct(productId: number) {
    this.http.deleteProductData(productId).subscribe(response=>
      {
        console.log('response from delete data is',response)
      });
  }

}
