import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../models/register.model';
import { FormGroup , FormBuilder , Validators ,FormControl } from '@angular/forms';
import { RegisterService } from '../register/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user : RegisterModel = new RegisterModel();
  registerForm : FormGroup;
  hide = true;
  alert : boolean=false;

  constructor(private formBuilder : FormBuilder, private http: RegisterService) { }

  ngOnInit(): void {
   this.registerForm = new FormGroup({
     userName: new FormControl('' , [Validators.required]),
     userEmail: new FormControl('' , [Validators.required,Validators.email]),
     number: new FormControl('' , [Validators.required]),
     password: new FormControl('' , [Validators.required,Validators.minLength(8),Validators.maxLength(15)]),
     roleId : new FormControl('' , [Validators.required])
    });
   }

   onRegisterSubmit(){
    this.http.postApiData(this.registerForm.value).subscribe((res) => {
      console.log('response from post data is ',res);
      this.alert = true;
    },(err) =>{
      console.log('error during post data is',err)
    })
    this.registerForm.reset({})
    alert('user added successfully!')
   }
   closealert(){
     this.alert = false;
   }
}
